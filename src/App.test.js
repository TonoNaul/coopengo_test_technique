import React from 'react';
import ReactDOM from 'react-dom';
import { render } from '@testing-library/react';
import App from './App';
import { act } from 'react-dom/test-utils';
import ReactTestUtils from 'react-dom/test-utils';
import { shallow, mount } from "enzyme";
import Case from './components/Case';
import Chart from './components/Case';
import Table from './components/Case';


const testTab=[{index:1, stocks:{CAC40:10, NASDAQ:10}},
  {index:2, stocks:{CAC40:20, NASDAQ:10}},
  {index:3, stocks:{CAC40:30, NASDAQ:10}},
  {index:4, stocks:{CAC40:40, NASDAQ:10}},
  {index:5, stocks:{CAC40:15, NASDAQ:10}},
  {index:6, stocks:{CAC40:25, NASDAQ:10}},
  {index:7, stocks:{CAC40:-20, NASDAQ:10}},
  {index:8, stocks:{CAC40:-10, NASDAQ:10}},
  {index:9, stocks:{CAC40:0, NASDAQ:10}},
  {index:10, stocks:{CAC40:10, NASDAQ:10}},
];

const resultTab=[{index:1, stocks:{CAC40:42, NASDAQ:10}},
  {index:2, stocks:{CAC40:20, NASDAQ:10}},
  {index:3, stocks:{CAC40:30, NASDAQ:10}},
  {index:4, stocks:{CAC40:40, NASDAQ:10}},
  {index:5, stocks:{CAC40:15, NASDAQ:10}},
  {index:6, stocks:{CAC40:25, NASDAQ:10}},
  {index:7, stocks:{CAC40:-20, NASDAQ:10}},
  {index:8, stocks:{CAC40:-10, NASDAQ:10}},
  {index:9, stocks:{CAC40:0, NASDAQ:10}},
  {index:10, stocks:{CAC40:10, NASDAQ:10}},
];

const updatedTable=[5,10];

let container;

beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  document.body.removeChild(container);
  container = null;
});

describe("App", () => {
  it("renders", () => {
    mount(<App />)
  });

  it("Input has the correct value",()=>{
    const app = mount(<App defaultTab={testTab}/>);
    const table = app.find(Table).first();
    const tabCase = table.find(Case).first();
    const input = tabCase.find("input");
    expect(input.prop("value")).toEqual(10);
  });

  it("Input update the value",()=>{
    const app = mount(<App defaultTab={testTab}/>);
    const table = app.find(Table).first();
    const tabCase = table.find(Case).first();
    const input = tabCase.find("input");

    input.simulate('change',{target:{value:"42"}});
    expect(table.prop('tab')).toEqual(resultTab);
  });

  it("Clicking on the button toggle pause",()=>{
    act(() => {
      ReactDOM.render(<App />, container);
    });
    const button = container.querySelector('button');
    expect(button.textContent).toEqual("PAUSE");
    act(() => {
      button.dispatchEvent(new MouseEvent('click', {bubbles: true}));
    });
    expect(button.textContent).toEqual("RESTART");
  });
});

describe("Case", () => {
  it("renders", () => {
    mount(<Case />)
  });

  it("renders with a value", () => {
    mount(<Case value={"123"}/>)
  });

  it("renders with an updated value ", () => {
    mount(<Case value={"123"} updated={true}/>)
  });
});

describe("Chart", () => {
  it("renders", () => {
    mount(<Chart />)
  });
});

describe("Table", () => {
  it("renders", () => {
    mount(<Table />)
  });

  it("renders with a tab", () => {
    mount(<Table tab={testTab} />)
  });

  it("renders with a tab of updated values", () => {
    mount(<Table tab={testTab} updatedCases={updatedTable}/>)
  });
});

