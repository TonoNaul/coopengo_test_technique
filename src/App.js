import React, { useState, useEffect,useRef } from 'react';
import {getLastStockValues} from "./API/api";
import Table from "./components/Table";
import StockChart from "./components/Chart";

function App(props) {
  const [stockValues, setStockValues] = useState(props.defaultTab||[]);
  const [play, setPlay] = useState(true);
  const [updatedIndex, setUpdatedIndex] = useState([]);

  function getStocks(num){
    getLastStockValues(num).then(data =>{
      setStockValues((data||[]).map((item,i)=>{
        return updatedIndex.find((elem)=>elem===item.index)?stockValues.find(stock=>stock.index===item.index):data[i]
      }));
    });
  }

  useEffect(()=>{
    getStocks(20);
  },[]);

  const togglePlay = () => {
    setPlay(!play);
  };

  const updateOneItemFromList = (value,index,type)=>{
    const list = stockValues.map((item) => {
      if (item.index === index) {
        return {
          index:index,
          stocks:{
            CAC40:type=="CAC40"?value:item.stocks.CAC40,
            NASDAQ:type=="NASDAQ"?value:item.stocks.NASDAQ
          },
          timestamp:item.timestamp
        }
      } else {
        return item;
      }
    });
    return list;
  };

  const handleChangeCase =(value,index,type)=>{
    const tmp = updateOneItemFromList(value,index,type);
    setUpdatedIndex(updatedIndex.find((elem)=>elem===index)?updatedIndex:[...updatedIndex,index]);
    setStockValues(tmp);
  };

  useInterval(()=>{
    if(play)getStocks(20);
  },1000);

  return (
    <div>
      <button onClick={togglePlay}>{play?"PAUSE":"RESTART"}</button>
      <Table tab={stockValues} togglePlay={togglePlay} handleChangeCase={handleChangeCase} updatedCases={updatedIndex}/>
      <StockChart tab={stockValues}/>
    </div>
  );
}

//From Dan Abramov
function useInterval(callback, delay) {
  const savedCallback = useRef();

  // Remember the latest callback.
  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  // Set up the interval.
  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

export default App;
