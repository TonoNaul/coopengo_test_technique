import React, {useEffect,useState } from 'react'
import { Chart } from 'react-charts'


export default function StockChart(props) {

  const [data, setData] = useState([]);
  let NASDAQStocks = [];
  let CAC40Stocks = [];

  useEffect(()=>{
    for(let i of props.tab){
      NASDAQStocks.push({x:new Date(i.timestamp),y:i.stocks.NASDAQ});
      CAC40Stocks.push({x:new Date(i.timestamp),y:i.stocks.CAC40});
    }

    setData([
          {
            label: 'Series 1',
            data: NASDAQStocks
          },
          {
            label: 'Series 2',
            data: CAC40Stocks
          }
        ]
    );
  },[props]);

  const axes = React.useMemo(
    () => [
      { primary: true, type: 'linear', position: 'bottom' },
      { type: 'linear', position: 'left' }
    ],
    []
  );

  return (
      <div style={{
        width: '800px',
        height: '500px',
        display:"inline-block"
      }}>
        <Chart data={data} axes={axes}/>
      </div>
  )
}