import React from 'react';

function Case(props) {
  return (
      <div style={{backgroundColor:props.edited?"#002244":"",...styles.default}}>
          <input value={props.value} onChange={(e)=>props.handleChange(e.target.value)} type={"number"}/>
      </div>
  );
}

const styles = {
  default:{
    padding:10,
    borderBottom:"solid 1px black",
    borderRight:"solid 1px black",
    borderLeft:"solid 1px black"
  }
};

export default Case;