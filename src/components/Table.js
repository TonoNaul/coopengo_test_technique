import React from 'react';
import Case from "./Case";

function Table(props) {
  return (
    <div style={{display:"inline-block"}}>
      {["CAC40","NASDAQ"].map((item)=>
        <div style={styles.column} key={item}>
          <div style={styles.topColumn}>{item}</div>
          {(props.tab||[]).map((i)=>
            <Case value={i.stocks[item]}
                  key={i.index}
                  handleChange={(e)=>{props.handleChangeCase(e,i.index,item)}}
                  edited={props.updatedCases.find(elem=>i.index===elem)?true:false}
            />
          )}
        </div>
      )}
    </div>
  );
}

const styles={
  topColumn:{
    padding:10,
    borderTop:"solid 1px black",
    borderRight:"solid 1px black",
    borderLeft:"solid 1px black",
    borderBottom:"solid 1px black"
  },
  column:{
    display:"inline-block"
  }
};

export default Table;